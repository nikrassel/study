import os

def print_dirs (project):
    print('\nСодержимое директории', project)
    for elem in os.listdir(project):
        path = os.path.join(project, elem)
        print('  ', path)

project_list = ['MyProjects', 'pythonProject']
for i_project in project_list:
    path_to_project = os.path.abspath(os.path.join('..', i_project))
    print_dirs (path_to_project)


file_name = 'admin.bat'
print('Абсолютный путь до файла:', os.path.abspath(file_name))
print ('Относительный путь до файла:', os.path.join('docs', 'My_projects', file_name))

print(os.path.abspath(os.path.join(os.path.sep)))
